package com.xnx3.wangmarket.plugin.cnzz;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * CNZZ 访问统计
 * @author 管雷鸣
 */
@PluginRegister(menuTitle = "CNZZ访问统计",menuHref="/plugin/cnzz/siteadmin/index.do", applyToCMS=true, intro="CNZZ网站访客统计，全方位统计网站访客情况。", version="1.0", versionMin="5.0")
public class Plugin{
}