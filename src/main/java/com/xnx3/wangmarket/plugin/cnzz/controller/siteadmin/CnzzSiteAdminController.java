package com.xnx3.wangmarket.plugin.cnzz.controller.siteadmin;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.EntityUtil;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.SessionUtil;
import com.xnx3.wangmarket.domain.bean.PluginMQ;
import com.xnx3.wangmarket.domain.mq.DomainMQ;
import com.xnx3.wangmarket.plugin.cnzz_domain.entity.Cnzz;
import net.sf.json.JSONObject;

/**
 * 网站管理后台
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/cnzz/siteadmin/")
public class CnzzSiteAdminController extends BasePluginController {
	@Resource
	private SqlService sqlService;

	/**
	 * 入口
	 */
	@RequestMapping("/index${url.suffix}")
	public String index(HttpServletRequest request, Model model) {
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		
		Cnzz cnzz = sqlService.findById(Cnzz.class, SessionUtil.getSite().getId());
		if(cnzz != null && cnzz.getIsUse() - Cnzz.IS_USE_YES == 0 && cnzz.getCnzzSiteid() != null && cnzz.getCnzzSiteid().length() > 2){
			//如果已经开启了，那么直接跳转到查看统计数据界面
			model.addAttribute("url", "see.do");
		}else{
			//没有设置完，进入设置界面
			model.addAttribute("url", "set.do");
		}
		
		return "plugin/cnzz/siteadmin/index";
	}
	
	/**
	 * 查看统计详情
	 */
	@RequestMapping("/see${url.suffix}")
	public String see(HttpServletRequest request, Model model) {
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		
		Cnzz cnzz = sqlService.findById(Cnzz.class, SessionUtil.getSite().getId());
		if(cnzz == null || cnzz.getIsUse() - Cnzz.IS_USE_NO == 0 || cnzz.getCnzzSiteid() == null || cnzz.getCnzzSiteid().length() < 2){
			return error(model, "尚未配置，请先进行配置", "plugin/cnzz/siteadmin/set.do");
		}
		
		//如果已经开启了，那么直接跳转到查看统计数据界面
		return redirect("http://new.cnzz.com/v1/login.php?siteid="+cnzz.getCnzzSiteid());
	}
	
	
	/**
	 * 网站管理后台设置页面
	 * @author 管雷鸣
	 */
	@RequestMapping("/set${url.suffix}")
	public String set(HttpServletRequest request, Model model) {
		if(!haveSiteAuth()){
			return error(model, "请先登录");
		}
		
		Site site = SessionUtil.getSite();
		
		Cnzz cnzz = sqlService.findById(Cnzz.class, site.getId());
		if(cnzz == null){
			//还没有，那么new一个
			cnzz = new Cnzz();
			cnzz.setSiteid(site.getId());
			cnzz.setIsUse(Cnzz.IS_USE_NO);//默认不使用
			cnzz.setCnzzSiteid("");
			sqlService.save(cnzz);
		}
		
		//日志
		ActionLogUtil.insert(request, "进入插件 cnzz 设置界面");
		
		model.addAttribute("cnzz", cnzz);
		return "plugin/cnzz/siteadmin/set";
	}
	
	/**
	 * 网站管理后台设置保存是否使用
	 * @param isUse 是否使用， 1使用， 0不使用
	 * @author 管雷鸣
	 */
	@ResponseBody
	@RequestMapping("/updateIsUse${url.suffix}")
	public BaseVO updateIsUse(HttpServletRequest request, Model model,
			@RequestParam(value = "isUse", required = false, defaultValue = "0") int isUse) {
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		
		Site site = SessionUtil.getSite();
		
		Cnzz cnzz = sqlService.findById(Cnzz.class, site.getId());
		if(cnzz == null){
			//还没有，那么new一个
			cnzz = new Cnzz();
			cnzz.setSiteid(site.getId());
			cnzz.setCnzzSiteid("");
		}
		cnzz.setIsUse(isUse == 1? Cnzz.IS_USE_YES:Cnzz.IS_USE_NO);//默认不使用
		sqlService.save(cnzz);
		
		//日志
		ActionLogUtil.insertUpdateDatabase(request, "插件 cnzz 修改 isUse 为"+(cnzz.getIsUse()-Cnzz.IS_USE_YES == 0? "使用":"不使用"));
		//MQ通知改动,向 domain 项目发送mq更新消息
		DomainMQ.send("cnzz", new PluginMQ(site).jsonAppend(JSONObject.fromObject(EntityUtil.entityToMap(cnzz))).toString());
		
		return success();
	}
	

	/**
	 * 网站管理后台设置 cnzz的站点id
	 * @param code 百度商桥的 https://hm.baidu.com/hm.js?98fecfe81b36aa1045d94554a4ac34a5 这个hm.js? 后面的这一串
	 * @author 管雷鸣
	 */
	@ResponseBody
	@RequestMapping("/updateCode${url.suffix}")
	public BaseVO updateCode(HttpServletRequest request, Model model,
			@RequestParam(value = "cnzzSiteid", required = false, defaultValue = "") String cnzzSiteid) {
		if(!haveSiteAuth()){
			return error("请先登录");
		}
		
		Site site = SessionUtil.getSite();
		
		Cnzz cnzz = sqlService.findById(Cnzz.class, site.getId());
		if(cnzz == null){
			//还没有，那么new一个
			cnzz = new Cnzz();
			cnzz.setSiteid(site.getId());
			cnzz.setIsUse(Cnzz.IS_USE_YES);
		}
		cnzz.setCnzzSiteid(cnzzSiteid);
		sqlService.save(cnzz);
		
		//日志
		ActionLogUtil.insertUpdateDatabase(request, "插件 cnzz 修改 cnzzSiteid 为"+StringUtil.filterXss(cnzzSiteid));
		//MQ通知改动,向 domain 项目发送mq更新消息
		DomainMQ.send("cnzz", new PluginMQ(site).jsonAppend(JSONObject.fromObject(EntityUtil.entityToMap(cnzz))).toString());
		
		return success();
	}
	
}