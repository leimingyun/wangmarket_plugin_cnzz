package com.xnx3.wangmarket.plugin.cnzz_domain;

import java.util.Map;

import com.xnx3.wangmarket.admin.pluginManage.interfaces.AutoLoadSimpleSitePluginTableDateInterface;
import com.xnx3.wangmarket.domain.bean.RequestInfo;
import com.xnx3.wangmarket.domain.bean.SimpleSite;
import com.xnx3.wangmarket.domain.pluginManage.interfaces.CreateReceiveMQInterface;
import com.xnx3.wangmarket.domain.pluginManage.interfaces.DomainVisitInterface;
import com.xnx3.wangmarket.domain.util.PluginCache;

/**
 * CNZZ统计
 * cnzz管理后台：https://web.umeng.com/main.php?c=site&a=show
 * @author 管雷鸣
 *
 */
public class Plugin implements DomainVisitInterface, AutoLoadSimpleSitePluginTableDateInterface, CreateReceiveMQInterface{
	//放在网页上面的js代码。其中 {id} 会自动替换为 cnzz 站点的id
	public static final String JS = "<div style=\"width:0px; height:0px; opacity: 0;display:none;\"><script type=\"text/javascript\" src=\"https://s96.cnzz.com/z_stat.php?id={id}&web_id={id}\"></script></div>";
	
	public String htmlManage(String html, SimpleSite simpleSite, RequestInfo requestInfo) {
		Map<String,Object> map = PluginCache.getPluginMap(simpleSite.getSiteid(), "cnzz");
		
		if(map == null){
			//为空，则是没有使用到该插件，将html 原样返回
			return html;
		}
		if(map.get("is_use") != null && map.get("is_use").toString().equals("1")){
			//如果当前网站启用了插件，那么就要向html末尾追加js
			Object cnzzSiteid = map.get("cnzz_siteid");
			if(cnzzSiteid != null){
				String js = JS.replaceAll("\\{id\\}", cnzzSiteid.toString());
				html = html + js;
			}
		}
		
		return html;
	}


	@Override
	public String autoLoadSimpleSitePluginTableDate() {
		return "SELECT * FROM plugin_cnzz";
	}


	@Override
	public void createReceiveMQForDomain() {
	}
	
}
