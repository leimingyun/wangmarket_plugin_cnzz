package com.xnx3.wangmarket.plugin.cnzz_domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.xnx3.j2ee.entity.BaseEntity;

/**
 * CNZZ 在数据表中存放的信息
 * @author 管雷鸣
 *
 */
@Entity
@Table(name = "plugin_cnzz")
public class Cnzz extends BaseEntity {
	/**
	 * 是否是在使用， 1使用
	 */
	public final static Short IS_USE_YES = 1;
	/**
	 * 是否是在使用， 0不使用
	 */
	public final static Short IS_USE_NO = 0;
	
	private Integer siteid;		//站点的id编号，对应 site.id，也是主健
	private Short isUse;		//是否是在使用， 1使用，0不使用
	private String cnzzSiteid;	//cnzz的站点id,通过 https://web.umeng.com/main.php?c=site&a=set 可以查看
	
	@Id
	@Column(name = "siteid", columnDefinition="int(11) comment '站点的id编号，对应 site.id' default '0' ")
	public Integer getSiteid() {
		return siteid;
	}
	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}
	
	@Column(name = "is_use", columnDefinition="int(11) comment '是否是在使用， 1使用，0不使用' default '0' ")
	public Short getIsUse() {
		return isUse;
	}
	public void setIsUse(Short isUse) {
		this.isUse = isUse;
	}
	
	@Column(name = "cnzz_siteid", columnDefinition="char(20) comment 'cnzz的站点id,通过 https://web.umeng.com/main.php?c=site&a=set 可以查看' default '0' ")
	public String getCnzzSiteid() {
		return cnzzSiteid;
	}
	public void setCnzzSiteid(String cnzzSiteid) {
		this.cnzzSiteid = cnzzSiteid;
	}
	
}