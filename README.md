
## 功能介绍
[wangmarket（网市场云建站系统）](https://gitee.com/mail_osc/wangmarket) 的 CNZZ访问统计 插件。即可看到统计数据。


## 使用条件
1. 本项目不能直接运行，需要放到 [网市场云建站系统](https://gitee.com/mail_osc/wangmarket) 中才可运行使用
1. 网市场云建站系统本身需要 v5.3 或以上版本。
1. 本项目只支持 mysql 数据库。使用默认 sqlite 数据库的不可用

## 使用方式
1. 下载 /target/ 中的jar包
1. 将下载的jar包放到 tomcat/webapps/ROOT/WEB-INF/lib/ 下
1. 重新启动运行项目，登陆网站管理后台，即可看到左侧的 功能插件 下多了名为 CNZZ访问统计 的功能插件。
1. 点开 CNZZ访问统计 的功能插件，即可开始使用。


## 二次开发
1. 运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 功能插件 ，即可开始使用。

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>


## 开发文档
[wm.zvo.cn](http://wm.zvo.cn)